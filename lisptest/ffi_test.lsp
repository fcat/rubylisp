;;; -*- mode: Scheme -*-

(context "ffi-instantiation-and-methods"
         ((define a (Array.)))
         (it "can manipulate objects"
             (.push a "hello")
             (assert-eq (.first a) "hello")))

(context "ffi-extension"
         ()
         (it " can add methods"
             (extend 'Object 'MyClass)
             (add-method 'MyClass 'foo (lambda () 42))
             (assert-eq (.foo (MyClass.)) 42)))
