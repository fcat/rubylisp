;;; -*- mode: Scheme -*-

(context "apply"
         ()
         (it "works"
             (assert-eq (apply + 4 '(1 2 3)) 10)
             (assert-eq (apply list '(1 2 3)) '(1 2 3))))
