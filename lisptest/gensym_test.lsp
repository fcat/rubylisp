;;; -*- mode: Scheme -*-

(context "gensym"
         ()
         (assert-eq (gensym) 'GENSYM-0)
         (assert-eq (gensym) 'GENSYM-1)
         (assert-eq (gensym "test") 'test-2)
         (assert-eq (gensym "test") 'test-3)
         (assert-eq (gensym) 'GENSYM-4)
         )
