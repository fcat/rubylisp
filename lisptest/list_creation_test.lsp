;;; -*- mode: Scheme -*-

(context "list"
         ()
         (assert-eq (list 1 2 3) '(1 2 3))
         (assert-eq (list (+ 1 1) (+ 2 2) (+ 3 3)) '(2 4 6)))

(context "cons"
         ()
         (assert-eq (cons 'a 'b) '(a . b))
         (assert-eq (cons 'a '(b c)) '(a b c)))

(context "cons*"
         ()
         (assert-eq (cons* 'a 'b 'c) '(a b . c))
         (assert-eq (cons* 'a 'b '(c d)) '(a b c d))
         (assert-eq (cons* 'a) 'a))

(context "make-list"
         ()
         (assert-eq (make-list 4 'c) '(c c c c)))

(context "iota"
         ()
         (assert-eq (iota 5) '(0 1 2 3 4))
         (assert-eq (iota 5 0 -3) '(0 -3 -6 -9 -12)))

