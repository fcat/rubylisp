;;; -*- mode: Scheme -*-

(context "reverse"
         ()
         (assert-eq (reverse '(a)) '(a))
         (assert-eq (reverse '(a b)) '(b a))
         (assert-eq (reverse '(a b c d)) '(d c b a)))

(context "map"
         ()
         (assert-eq (map (lambda (x) (+ x 1)) '(1 2 3 4)) '(2 3 4 5))
         (assert-eq (map + '(1 2 3 4) '(1 1 1 1)) '(2 3 4 5))
         (assert-eq (map (lambda (x) (list x x)) '(a b c)) '((a a) (b b) (c c)))
         (assert-eq (map cadr '((a b) (d e) (g h))) '(b e h)))

(context "reduce"
         ()
         (assert-eq (reduce-left + 0 '(1 2 3 4)) 10)
         (assert-eq (reduce-left + 0 '()) 0)
         (assert-eq (reduce-left + 0 '(1)) 1)
         (assert-eq (reduce-left * 1 '(2 3 4)) 24)
         (assert-eq (reduce-left list '() '(1 2 3 4)) '(((1 2) 3) 4))
         (assert-eq (reduce-left list '() '()) '())
         (assert-eq (reduce-left list '() '(1)) '1)
         (assert-eq (reduce-left (lambda (acc x) (list acc x)) '() '(1 2 3 4)) '(((1 2) 3) 4)))

(context "append"
         ((define a '(1 2 3))
          (define b '(4 5 6)))
         (assert-eq (append '(1 2 3) '(4 5 6)) '(1 2 3 4 5 6))
         (assert-eq (append a b) '(1 2 3 4 5 6))
         (assert-eq a '(1 2 3))
         (assert-eq b '(4 5 6))
         (assert-eq (append '(1 2) '(3 4) '(5 6)) '(1 2 3 4 5 6)))

(context "append!"
         ((define a '(1 2 3))
          (define b '(4 5 6))
          (define c '(7 8 9)))
         (assert-eq (append! '(1 2 3) '(4 5 6)) '(1 2 3 4 5 6))
         (assert-eq (append! a b c) '(1 2 3 4 5 6 7 8 9))
         (assert-eq a '(1 2 3 4 5 6 7 8 9))
         (assert-eq b '(4 5 6 7 8 9))
         (assert-eq c '(7 8 9)))

(context "filter"
         ()
         (assert-eq (filter odd? '(1 2 3 4 5 6)) '(1 3 5)))

(context "remove"
         ()
         (assert-eq (remove odd? '(1 2 3 4 5 6)) '(2 4 6)))

(context "partition"
         ()
         (assert-eq (partition odd? '(1 2 3 4 5 6)) '((1 3 5) (2 4 6))))

(context "searching"
         ()
         (assert-eq (memq 'a '(a b c)) '(a b c))
         (assert-eq (memq 'b '(a b c)) '(b c))
         (assert-eq (memq 'a '(b c d)) #f)
         (assert-eq (memq '(a) '(b (a) c)) #f)
         (assert-eq (member (list 'a) '(b (a) c)) '((a) c))
         (assert-eq (memv 101 '(100 101 102)) '(101 102)))

(context "any"
         ()
         (assert-true (any integer? '(a 3 b 2.7)))
         (assert-false (any integer? '(a 3.1 b 2.7)))
         (assert-true (any < '(3 1 4 1 5)
                           '(2 7 1 8 2))))

(context "every"
         ()
         (assert-true (every integer? '(4 3 8 27)))
         (assert-false (every integer? '(a 3 b 7)))
         (assert-true (every < '(3 1 4 1 0)
                               '(5 7 9 8 2))))
