;;; -*- mode: Scheme -*-

(context "do"
         ()
         (assert-eq (do ((x 0 (+ x 1)))
                        ((= x 10) x))
                    10))
