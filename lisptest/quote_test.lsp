;;; -*- mode: Scheme -*-

(context "quote"
         ()
         (assert-eq (quote 1) 1)
         (assert-true (list? (quote (+ 1 2))))
         (assert-neq (quote (+ 1 2)) 3))
