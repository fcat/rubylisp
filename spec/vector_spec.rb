describe "new vector" do

  before do
    @v = Lisp::Vector.new()
  end
  
  it "is empty" do
    expect(@v).to be_empty
  end

  it "has no elements" do
    expect(@v.length).to eq 0
  end

  it "prints correctly" do
    expect(@v.to_s).to eq "#()"
  end
  
end


describe "non-empty vector" do

  before do
    @v = Lisp::Vector.new()
    @v.add(Lisp::Number.with_value(1))
    @v.add(Lisp::Number.with_value(2))
  end

  it "is not empty" do
    expect(@v).to_not be_empty
  end

  it "has 2 elements" do
    expect(@v.length).to eq 2
  end

  it "has the correct elements" do
    expect(@v.at(0).value).to eq 1
    expect(@v.at(1).value).to eq 2
  end

  it "prints correctly" do
    expect(@v.to_s).to eq "#(1 2)"
  end

  it "updates properly" do
    @v.at_put(0, Lisp::Number.with_value(42))
    expect(@v.at(0).value).to eq 42
  end
  
end

