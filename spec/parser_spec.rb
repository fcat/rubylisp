describe "Parser" do

  before do
    @p = Lisp::Parser.new
  end

  it "parses a number" do
    sexpr = @p.parse("5")
    expect(sexpr.type).to eq :number
    expect(sexpr.value).to eq 5
  end

  it "parses another number" do
    sexpr = @p.parse("467")
    expect(sexpr.type).to eq :number
    expect(sexpr.value).to eq 467
  end

  it "parses a negative number" do
    sexpr = @p.parse("-467")
    expect(sexpr.type).to eq :number
    expect(sexpr.value).to eq -467
  end

  it "parses a hex number" do
    sexpr = @p.parse("#xa5")
    expect(sexpr.type).to eq :number
    expect(sexpr.value).to eq 165
  end

  it "parses an upper hex number" do
    sexpr = @p.parse("#xA5")
    expect(sexpr.type).to eq :number
    expect(sexpr.value).to eq 165
  end

  it "parses an mixed case hex number" do
    sexpr = @p.parse("#xAf")
    expect(sexpr.type).to eq :number
    expect(sexpr.value).to eq 175
  end

  it "parses a floating point number" do
    sexpr = @p.parse("5.42")
    expect(sexpr.type).to eq :number
    expect(sexpr.value).to eq 5.42
  end

  it "parses a negative floating point number" do
    sexpr = @p.parse("-5.42")
    expect(sexpr.type).to eq :number
    expect(sexpr.value).to eq -5.42
  end

  it "parses a string" do
    sexpr = @p.parse('"test"')
    expect(sexpr.type).to eq :string
    expect(sexpr.value).to eq 'test'
  end

  it "parses another string" do
    sexpr = @p.parse('"Lots of Stylish Parentheses"')
    expect(sexpr.type).to eq :string
    expect(sexpr.value).to eq 'Lots of Stylish Parentheses'
  end

  it "parses another string" do
    sexpr = @p.parse('"Lots of Stylish Parentheses"')
    expect(sexpr.type).to eq :string
    expect(sexpr.value).to eq 'Lots of Stylish Parentheses'
  end

  it "parses simple character" do
    sexpr = @p.parse('#\a')
    expect(sexpr.type).to eq :character
    expect(sexpr.value).to eq "a"
  end

  it "parses named character" do
    sexpr = @p.parse('#\space')
    expect(sexpr.type).to eq :character
    expect(sexpr.value).to eq " "
  end

  it "parses character code" do
    sexpr = @p.parse('#\U+61')
    expect(sexpr.type).to eq :character
    expect(sexpr.value).to eq "a"
  end

  it "parses true" do
    sexpr = @p.parse('#t')
    expect(sexpr.type).to eq :boolean
    expect(sexpr.value).to be true
  end

  it "parses false" do
    sexpr = @p.parse('#f')
    expect(sexpr.type).to eq :boolean
    expect(sexpr.value).to be false
  end

  it "parses a symbol" do
    sexpr = @p.parse('test')
    expect(sexpr.type).to eq :symbol
    expect(sexpr.name).to eq 'test'
  end

  it "parses another symbol" do
    sexpr = @p.parse('defun')
    expect(sexpr.type).to eq :symbol
    expect(sexpr.name).to eq 'defun'
  end

  it "parses an FFI new symbol" do
    sexpr = @p.parse('Object. ')
    expect(sexpr.type).to eq :primitive
    expect(sexpr.value).to eq 'Object'
  end

  it "parses an FFI send symbol" do
    sexpr = @p.parse('.test')
    expect(sexpr.type).to eq :primitive
    expect(sexpr.value).to eq :test
  end

  it "parses empty list" do
    sexpr = @p.parse('()')
    expect(sexpr).to_not be_nil
    expect(sexpr.car).to be_nil
    expect(sexpr.cdr).to be_nil
    expect(sexpr.length).to eq 0
  end

  it "parses number car" do
    sexpr = @p.parse('(1)')
    expect(sexpr).to_not be_nil
    expect(sexpr.type).to eq :pair
    expect(sexpr.car.type).to eq :number
    expect(sexpr.car.value).to eq 1
  end

  it "parses string car" do
    sexpr = @p.parse('("hello")')
    expect(sexpr).to_not be_nil
    expect(sexpr.type).to eq :pair
    expect(sexpr.car.type).to eq :string
    expect(sexpr.car.value).to eq "hello"
  end

  it "parses multi element list" do
    sexpr = @p.parse('(1 2)')
    expect(sexpr).to_not be_nil
    expect(sexpr.type).to eq :pair
    expect(sexpr.cdr.type).to eq :pair
    expect(sexpr.cddr).to be_nil
    expect(sexpr.car.type).to eq :number
    expect(sexpr.car.value).to eq 1
    expect(sexpr.cadr.type).to eq :number
    expect(sexpr.cadr.value).to eq 2
  end

  it "parses a nested list" do
    sexpr = @p.parse('(1 (2 3) 4)')
    expect(sexpr).to_not be_nil
    expect(sexpr.type).to eq :pair

    expect(sexpr.car.type).to eq :number
    expect(sexpr.car.value).to eq 1

    expect(sexpr.cadr.type).to eq :pair
    expect(sexpr.caadr.type).to eq :number
    expect(sexpr.caadr.value).to eq 2
    expect(sexpr.cadadr.type).to eq :number
    expect(sexpr.cadadr.value).to eq 3
    expect(sexpr.cddadr).to be_nil

    expect(sexpr.caddr.type).to eq :number
    expect(sexpr.caddr.value).to eq 4

    expect(sexpr.cdddr).to be_nil
  end

  it "parses dotted pair" do
    sexpr = @p.parse('(1 . 2)')
    expect(sexpr).to_not be_nil
    expect(sexpr.type).to eq :pair

    expect(sexpr.car.type).to eq :number
    expect(sexpr.car.value).to eq 1
    expect(sexpr.cdr.type).to eq :number
    expect(sexpr.cdr.value).to eq 2
  end

  it "parses functional format" do
    sexpr = @p.parse('(+ 1 2)')
    expect(sexpr).to_not be_nil

    expect(sexpr.car.type).to eq :symbol
    expect(sexpr.car.name).to eq '+'
    expect(sexpr.cadr.type).to eq :number
    expect(sexpr.cadr.value).to eq 1
    expect(sexpr.caddr.type).to eq :number
    expect(sexpr.caddr.value).to eq 2
  end

  it "parses a quote expression" do
    sexpr = @p.parse("'a")
    expect(sexpr).to_not be_nil

    expect(sexpr.type).to eq :pair
    expect(sexpr.car.type).to eq :symbol
    expect(sexpr.car.name).to eq 'quote'
    expect(sexpr.cadr.type).to eq :symbol
    expect(sexpr.cadr.value).to eq 'a'
  end

  it "parses a quasiquote expression" do
    sexpr = @p.parse("`a")
    expect(sexpr).to_not be_nil

    expect(sexpr.type).to eq :pair
    expect(sexpr.car.type).to eq :symbol
    expect(sexpr.car.name).to eq 'quasiquote'
    expect(sexpr.cadr.type).to eq :symbol
    expect(sexpr.cadr.value).to eq 'a'
  end

  it "parses a unquote expression" do
    sexpr = @p.parse(",a")
    expect(sexpr).to_not be_nil

    expect(sexpr.type).to eq :pair
    expect(sexpr.car.type).to eq :symbol
    expect(sexpr.car.name).to eq 'unquote'
    expect(sexpr.cadr.type).to eq :symbol
    expect(sexpr.cadr.value).to eq 'a'
  end

  it "parses a unquote-spliceing expression" do
    sexpr = @p.parse(",@a")
    expect(sexpr).to_not be_nil

    expect(sexpr.type).to eq :pair
    expect(sexpr.car.type).to eq :symbol
    expect(sexpr.car.name).to eq 'unquote-splicing'
    expect(sexpr.cadr.type).to eq :symbol
    expect(sexpr.cadr.value).to eq 'a'
  end


  it "parses a frame" do
    sexpr = @p.parse("{a: 1}")
    expect(sexpr).to_not be_nil
    expect(sexpr.type).to eq :pair
    expect(sexpr.length).to eq 3
    
    expect(sexpr.car.type).to eq :symbol
    expect(sexpr.car.name).to eq 'make-frame'

    expect(sexpr.cadr.type).to eq :symbol
    expect(sexpr.cadr.name).to eq 'a:'

    expect(sexpr.caddr.type).to eq :number
    expect(sexpr.caddr.value).to eq 1
  end


  it "parses a vector" do
    sexpr = @p.parse("#(1 2)")
    expect(sexpr).to_not be_nil
    expect(sexpr.type).to eq :pair
    expect(sexpr.length).to eq 3
    
    expect(sexpr.car.type).to eq :symbol
    expect(sexpr.car.name).to eq 'vector'

    expect(sexpr.cadr.type).to eq :number
    expect(sexpr.cadr.value).to eq 1

    expect(sexpr.caddr.type).to eq :number
    expect(sexpr.caddr.value).to eq 2
  end
  
    
  it "parses a literal vector" do
    sexpr = @p.parse("'#(1 2)")
    expect(sexpr).to_not be_nil
    expect(sexpr.type).to eq :vector
    expect(sexpr.length).to eq 2
    
    expect(sexpr.at(0).type).to eq :number
    expect(sexpr.at(0).value).to eq 1

    expect(sexpr.at(1).type).to eq :number
    expect(sexpr.at(1).value).to eq 2
  end
  
    
end
