describe 'running lisp tests' do

  it 'passes tests' do
    test_command = '(run-all-tests "lisptest")'
    parser = Lisp::Parser.new
    parser.process_file("lisp/testing.lsp") 
    result = parser.parse_and_eval(test_command)
    expect(result.value).to be true
  end

end
