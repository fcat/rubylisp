describe 'Number' do

  it "defaults to zero" do
    n = Lisp::Number.new
    expect(n.value).to eq 0
  end

  it "takes the given value" do
    n = Lisp::Number.with_value(5)
    expect(n.value).to eq 5
  end

  it "lets you set it's value" do
    n = Lisp::Number.new()
    n.set!(5)
    expect(n.value).to eq 5
  end

  it "knows what it is" do
    n = Lisp::Number.new
    expect(n.number?).to be true
    expect(n.type).to eq :number
  end
  
  it "knows what it isn't" do
    n = Lisp::Number.new
    expect(n.string?).to be false
    expect(n.symbol?).to be false
    expect(n.pair?).to be false
  end

  it "converts to a string" do
    n = Lisp::Number.with_value(5)
    expect(n.to_s).to eq '5'
  end

end
