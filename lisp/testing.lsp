;;; -*- mode: Scheme -*-

;;; Copyright 2015 SteelSeries ApS. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

(define number-of-passes 0)
(define number-of-failures 0)
(define number-of-errors 0)

(define failure-messages '())
(define error-messages '())

(define verbosity 'terse)
(define context-name "")
(define it-name "")

(define (verbose?) (eq? verbosity 'verbose))
(define (terse?) (eq? verbosity 'terse))
(define (quiet?) (eq? verbosity 'quiet))

(define (reset-testing)
  (set! number-of-passes 0)
  (set! number-of-failures 0)
  (set! number-of-errors 0)
  (set! failure-messages '())
  (set! error-messages '())
  (set! verbosity 'terse)
  (format #t "~%~%Running lisp tests~%"))

(define (log-pass msg)
  (set! number-of-passes (succ number-of-passes))
  (cond ((verbose?)
         (format #t "    ~S~%" msg))
        ((terse?)
         (format #t "+"))))


(define (log-failure prefix msg)
  (set! number-of-failures (succ number-of-failures))
  (let ((failure-message (format #f "~S ~S:~%    ~S~%      - ~S" context-name it-name prefix msg)))
    (set! failure-messages (cons failure-message failure-messages))
    (cond ((verbose?)
           (format #t "    ~S~%      - ~S~%" prefix msg))
          ((terse?)
           (format #t "-")))))

(define (log-error err)
  (set! number-of-errors (succ number-of-errors))
  (let ((error-message (format #f "~S ~S:~%    ERROR: ~S" context-name it-name err)))
    (set! error-messages (cons error-message error-messages))
    (cond ((verbose?)
           (format #t "    ERROR: ~S~%" err))
          ((terse?)
           (format #t "!")))))

(defmacro (context label setup . body)
  (if (not (or (symbol? label) (string? label)))
      (error "The label of a describe must be a symbol or string.")
      `(begin (when (verbose?)
                    (format #t "~%~S~%" ,label))
              (set! context-name ,label)
              (set! it-name "")
              (for-each (lambda (it-clause)
                          ,@setup
                          (eval it-clause))
                        ',body))))

(defmacro (it label . body)
  (if (not (or (symbol? label) (string? label)))
      (error "The label of a describe must be a symbol or string.")
      `(begin (when (verbose?)
                    (format #t "~%  ~S~%" ,label))
              (set! it-name ,label)
              (on-error (begin ,@body)
                        (lambda (err)
                          (format #t "Encountered error: ~S" err)
                          (let* ((err-parts (string-split err "\n"))
                                 (last-line (car (last-pair err-parts))))
                            (log-error last-line)))))))

(defmacro (assert-true sexpr)
  `(let ((actual ,sexpr)
         (msg (format #f "(assert-true ~A)" ',sexpr)))
     (if actual
         (log-pass msg)
         (log-failure msg "expected true, but was false"))))


(defmacro (assert-false sexpr)
  `(let ((actual ,sexpr)
         (msg (format #f "(assert-false ~A)" ',sexpr)))
     (if (not actual)
         (log-pass msg)
         (log-failure msg "expected false, but was true"))))


(defmacro (assert-nil sexpr)
  `(let ((actual ,sexpr)
         (msg (format #f "(assert-null ~A)" ',sexpr)))
     (if (nil? actual)
         (log-pass msg)
         (log-failure msg "expected nil, but wasn't"))))


(defmacro (assert-not-nil sexpr)
  `(let ((actual ,sexpr)
         (msg (format #f "(assert-not-null ~A)" ',sexpr)))
     (if (not (nil? actual))
         (log-pass msg)
         (log-failure msg "expected not nil, but was"))))


(defmacro (assert-eq sexpr expected-sexpr)
  `(let* ((actual ,sexpr)
          (expected ,expected-sexpr)
          (msg (format #f "(assert-eq ~A ~A)" ',sexpr ',expected-sexpr)))
     (if (equal? actual expected)
         (log-pass msg)
         (log-failure msg (format #f "expected ~A, but was ~A" expected actual)))))


(defmacro (assert-neq sexpr expected-sexpr)
  `(let* ((actual ,sexpr)
          (expected ,expected-sexpr)
          (msg (format #f "(assert-neq ~A ~A)" ',sexpr ',expected-sexpr)))
     (if (not (equal? actual expected))
         (log-pass msg)
         (log-failure msg (format #f "did not expect ~A, but it was" expected)))))

(defmacro (assert-error **sexpr**)
  `(let ((msg (format #f "(assert-error ~A)" ',**sexpr**)))
     (on-error ,**sexpr**
               (lambda (err)
                 (log-pass msg))
               (lambda ()
                 (log-failure msg "expected an error, but there wasn't")))))

(defmacro (assert-nerror **sexpr**)
  `(let ((msg (format #f "(assert-nerror ~A)" ',**sexpr**)))
     (on-error ,**sexpr**
               (lambda (err)
                 (log-failure msg (format #f "expected no error, but error was ~A" err)))
               (lambda ()
                 (log-pass msg)))))

(defmacro (assert-memq sexpr object)
  `(let* ((searched-for ,object)
          (result (memq ,object ,sexpr))
          (msg (format #f "(assert-memq ~A ~S)" ',sexpr ',object)))
     (if result
         (log-pass msg)
         (log-failure msg (format #f "expected ~A to contain ~S, but it didn't" ',sexpr searched-for)))))

(define (dump-summary duration)
  (format #t "~%Ran ~A lisp tests in ~A seconds~%"
          (+ number-of-passes number-of-failures number-of-errors)
          duration)
  (format #t "~A passes, ~A failures, ~A errors~%"
          number-of-passes
          number-of-failures
          number-of-errors)
  (unless (zero? number-of-failures)
    (format #t "~%Failures:~%")
    (for-each (lambda (m) (format #t "  ~S~%" m))
              failure-messages))
  (unless (zero? number-of-errors)
    (format #t "~%Errors:~%")
    (for-each (lambda (m) (format #t "  ~S~%" m))
              error-messages))
  (format #t "~%")
  (and (zero? number-of-failures) (zero? number-of-errors)))

(define (run-all-tests test-dir . optionals)
  (reset-testing)
  (set! verbosity (if (null? optionals)
                      'terse
                      (car optionals)))
  (let* ((files (list-directory test-dir "*_test.lsp"))
         (t (time (for-each load files))))
    (dump-summary t)))

(define (run-test test-file . optionals)
  (reset-testing)
  (set! verbosity (if (null? optionals)
                      'terse
                      (car optionals)))
  (let ((t (time (load test-file))))
    (dump-summary t)))
