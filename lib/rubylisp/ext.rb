class NilClass

  def true?
    false
  end
  
  def false?
    false
  end

  def to_s
    "()"
  end

  
  def to_ary
    []
  end

  
  def eqv?(other)
    other.nil? || (other.pair? && other.empty?)
  end
  
  def eq?(other)
    other.nil? || (other.pair? && other.empty?)
  end
  
  def equal?(other)
    other.nil? || (other.pair? && other.empty?)
  end
  
  def print_string
    self.to_s
  end

  def print_string_helper
    ""
  end

  def evaluate(env)
    nil
  end

  def evaluate_each(env)
    nil
  end

  def car
    nil
  end

  def cdr
    nil
  end

  def set_car!(s)
    nil
  end
  
  def set_cdr!(s)
    nil
  end

  def quoted
    nil
  end
  
  def empty?
    true
  end

  def length
    0
  end
  
  def value
    nil
  end

  def string?
    false
  end
  
  def number?
    false
  end
  
  def symbol?
    false
  end
  
  def pair?
    false
  end
  
  def list?
    true
  end
  
  def vector?
    false
  end
  
  def environment?
    false
  end
  
  def method_missing(name, *args, &block)
    is_list_walk = name[0] == ?c && name[-1] == ?r && (name[1..-2].chars.all? {|e| "ad".include?(e)})
    if is_list_walk
      nil
    else
      #puts "nil#method_missing name: #{name} args #{args}"
      #puts caller
      super
    end
  end
  
  def primitive?
    false
  end
  
  def function?
    false
  end

  def object?
    false
  end

  def type
    :nil
  end

  def lisp_object?
    true
  end
    
  def flatten
    nil
  end  
end


class Object

  def lisp_onject?
    false
  end
    
end
