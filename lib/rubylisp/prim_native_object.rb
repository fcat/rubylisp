module Lisp

  class PrimNativeObject

    def self.register
      Primitive.register("wrap-object", "1")  {|args, env| Lisp::NativeObject::wrap_impl(args, env) }
    end

    def self.wrap_impl(args, env)
      raw_val = args.car.evaluate(env)
      val = if raw_val.list?
              raw_val.to_a
            else
              raw_val
            end
      NativeObject.with_value(val)
    end
    
  end

end
