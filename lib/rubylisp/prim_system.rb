module Lisp

  class PrimSystem

    def self.register
      Primitive.register("sleep", "1") {|args, env| Lisp::PrimSystem.sleep_impl(args, env) }
      Primitive.register("time", "1", "", true)  {|args, env| Lisp::PrimSystem.time_impl(args, env) }
      Primitive.register("quit", "0")  {|args, env| exit() }
      Primitive.register("error", "1")  {|args, env| Lisp::PrimSystem.error_impl(args, env) }
      Primitive.register("on-error", "2|3", "", true)  {|args, env| Lisp::PrimSystem.on_error_impl(args, env) }
    end

    
    def self.sleep_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("sleep needs a numeric argument", env) unless arg.number?
      sleep(arg.value)
    end

    
    def self.time_impl(args, env)
      start_time = Time.now
      args.car.evaluate(env)
      end_time = Time.now
      Lisp::Number.with_value(end_time - start_time)
    end    


    def self.error_impl(args, env)
      #puts "error #{args.car.print_string}"
      Lisp::Debug.process_error(args.car.to_s, env)
    end
    

    def self.on_error_impl(args, env)
      #puts "on-error ===> #{args.car.body.print_string}"
      begin
        result = args.car.evaluate(env)
      rescue => e
        handler = args.cadr.evaluate(env)
        return Lisp::Debug.process_error("on-error needs a function as it's second argument", env) unless handler.function?
        #puts "ERROR: #{e}"
        err_string = Lisp::String.with_value("#{e}")
        handler.apply_to(Lisp::ConsCell.array_to_list([err_string]), env)
      else
        if args.length == 3
          handler = args.caddr.evaluate(env)
          return Lisp::Debug.process_error("on-error needs a function as it's third argument", env) unless handler.function?
          handler.apply_to(nil, env)
        end
      end
    end
    
  end
end
