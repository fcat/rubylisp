module Lisp

  class PrimMath

    def self.register
      self.bind("PI", ::Math::PI)
      self.bind("E", ::Math::E)
      
      Primitive.register("+", ">=1", "(+ number...)\n\nAdds a series of numbers.") do |args, env|
        Lisp::PrimMath.add_impl(args, env)
      end
      
      Primitive.register("-", ">=1", "(- number...)\n\nSequentially subtracts a sequence of numbers.\nAs expected, a unary form of - is available as well.") do |args, env|
        Lisp::PrimMath.subtract_impl(args, env)
      end

      Primitive.register("*", ">=1", "(* number...)\n\nMultiplies a series of numbers.") do |args, env|
        Lisp::PrimMath.multiply_impl(args, env)
      end

      Primitive.register("/", ">=1", "(/ number...)\n\nSequentially divides a sequence of numbers.") do |args, env|
        Lisp::PrimMath.quotient_impl(args, env)
      end

      Primitive.register("%", "1|2", "(% number number)\n\nReturns the remainder of the division of two numbers. NOTE: modulus only works for integers.") do |args, env|
        Lisp::PrimMath.remainder_impl(args, env)
      end

      Primitive.register("modulo", "1|2", "(modulo number number)\n\nReturns the remainder of the division of two numbers. NOTE: modulus only works for integers.") do |args, env|
        Lisp::PrimMath.remainder_impl(args, env)
      end

      Primitive.register("even?", "1", "(even? number)\n\nReturns whether the argument is even.") do |args, env|
        Lisp::PrimMath.even_impl(args, env)
      end

      Primitive.register("odd?", "1", "(odd? number)\n\nReturns whether the argument is odd.") do |args, env|
        Lisp::PrimMath.odd_impl(args, env)
      end

      Primitive.register("zero?", "1", "(zero? number)\n\nReturns whether the argument is zero.") do |args, env|
        Lisp::PrimMath.zero_impl(args, env)
      end

      Primitive.register("positive?", "1", "(positive? number)\n\nReturns whether the argument is positive.") do |args, env|
        Lisp::PrimMath.positive_impl(args, env)
      end

      Primitive.register("negative?", "1", "(negative? number)\n\nReturns whether the argument is negative.") do |args, env|
        Lisp::PrimMath.negative_impl(args, env)
      end

      Primitive.register("interval", "2", "(interval lo hi)\n\nCreates a list of numbers from `lo` to `hi`, inclusive.") do |args, env|
        Lisp::PrimMath.interval_impl(args, env)
      end

      Primitive.register("truncate", "1", "(truncate number)\n\nReturns the integer value of number. If it is an integer, it is simply returned. However, if it is a float the integer part is returned.") do |args, env|
        Lisp::PrimMath.truncate_impl(args, env)
      end

      Primitive.register("round", "1", "(round number)\n\nIf number is an integer, it is simply returned. However, if it is a float the closest integer is returned.") do |args, env|
        Lisp::PrimMath.round_impl(args, env)
      end

      Primitive.register("ceiling", "1", "(ceiling number)\n\nIf `number` is an integer, it is simply returned. However, if it is a float the smallest integer greater than or equal to `number` is returned.") do |args, env|
        Lisp::PrimMath.ceiling_impl(args, env)
      end

      Primitive.register("floor", "1", "(floor number)\n\nIf `number` is an integer, it is simply returned. However, if it is a float the
largest integer less than or equal to `number` is returned.") do |args, env|
        Lisp::PrimMath.floor_impl(args, env)
      end

      Primitive.register("random", "0|1", "(random)\n\nReturns a pseudo-random floating point number between 0.0 and 1.0, including 0.0 and excluding 1.0.\n\n(random n)\n\nReturns a pseudo-random integer greater than or equal to 0 and less than n.") do |args, env|
        Lisp::PrimMath.random_impl(args, env)
      end

      Primitive.register("float", "1", "(float number)\n\nReturns the floating point value of number. If it is a float, it is simply returned. However, if it is an integer it is converted to float and returned.") do |args, env|
        Lisp::PrimMath.float_impl(args, env)
      end

      Primitive.register("integer", "1", "(integer number)\n\nReturns the integer value of number. If it is an integer, it is simply returned. However, if it is a float the integer part is returned. This is the same as tuncate.") do |args, env|
        Lisp::PrimMath.integer_impl(args, env)
      end

      Primitive.register("sqrt", "1", "(sqrt number)\n\nReturns the square root of `number'.") do |args, env|
        Lisp::PrimMath.sqrt_impl(args, env)
      end

      Primitive.register("min", ">=1", "(min number...)\n\nReturns the smallest of all the `number` arguments.") do |args, env|
        Lisp::PrimMath.min_impl(args, env)
      end

      Primitive.register("max", ">=1", "(max number...)\n\nReturns the largest of all the `number` arguments.") do |args, env|
        Lisp::PrimMath.max_impl(args, env)
      end

      Primitive.register("abs", "1", "(abs number)\n\nReturns the absolute value of `number'.") do |args, env|
        Lisp::PrimMath.abs_impl(args, env)
      end

      Primitive.register("sin", "1", "(sin number)\n\nReturns the sine of `number'.") do |args, env|
        Lisp::PrimMath.sin_impl(args, env)
      end

      Primitive.register("cos", "1", "(cos number)\n\nReturns the cosine of `number'.") do |args, env|
        Lisp::PrimMath.cos_impl(args, env)
      end

      Primitive.register("tan", "1", "(tan number)\n\nReturns the tangent of `number'.") do |args, env|
        Lisp::PrimMath.tan_impl(args, env)
      end

      Primitive.register("pred", "1", "(pred number)\n\nReturns one less than `number'.") do |args, env|
        Lisp::PrimMath.pred_impl(args, env)
      end
      
      Primitive.register("succ", "1", "(succ number)\n\nReturns one more than `number'.") do |args, env|
        Lisp::PrimMath.succ_impl(args, env)
      end

      
    end


    def self.bind(name, number)
      EnvironmentFrame.global.bind(Symbol.named(name), Number.with_value(number))
    end

    def self.add_impl(args, env)
      acc = 0
      c = args
      while !c.nil?
        n = c.car
        return Lisp::Debug.process_error("add needs number arguments but was given a #{n.type}: #{n}", env) unless n.type == :number
        acc += n.value
        c = c.cdr
      end

      Number.with_value(acc)
    end


    def self.subtract_impl(args, env)
      return Number.with_value(-1 * args.car.value) if args.length == 1

      first = args.car
      return Lisp::Debug.process_error("subtract needs number arguments, but received #{first}", env)  unless first.number?
      acc = first.value
      c = args.cdr
      while !c.nil?
        n = c.car
        return Lisp::Debug.process_error("subtract needs number arguments, but received #{n}", env) unless n.number?
        acc -= n.value
        c = c.cdr
      end

      Number.with_value(acc)
    end


    def self.multiply_impl(args, env)
      acc = 1
      c = args
      while !c.nil?
        n = c.car
        return Lisp::Debug.process_error("multiply needs number arguments, but received #{n}", env) unless n.number?
        acc *= n.value
        c = c.cdr
      end

      Number.with_value(acc)
    end


    def self.quotient_impl(args, env)
      first = args.car
      return Lisp::Debug.process_error("quotient needs number arguments, but received #{first}", env) unless first.number?
      return first if args.length == 1
      acc = first.value
      c = args.cdr
      while !c.nil?
        n = c.car
        return Lisp::Debug.process_error("quotient needs number arguments, but received #{n}", env) unless n.number?
        return Lisp::Debug.process_error("divide by zero", env) if n.value == 0
        acc /= n.value
        c = c.cdr
      end

      Number.with_value(acc)
    end


    def self.remainder_impl(args, env)
      first = args.car
      return Lisp::Debug.process_error("remainder needs number arguments, but received #{first}", env) unless first.number?
      return first if args.length == 1
      acc = first.value
      c = args.cdr
      while !c.nil?
        n = c.car
        return Lisp::Debug.process_error("remainder needs number arguments, but received #{n}", env) unless n.number?
        return Lisp::Debug.process_error("divide by zero", env) if n.value == 0
        acc %= n.value
        c = c.cdr
      end

      Number.with_value(acc)
    end

    def self.truncate_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("truncate needs a number argument, but received #{arg}", env) unless arg.number?
      Number.with_value(arg.value.truncate)
    end


    def self.round_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("round needs a number argument, but received #{arg}", env) unless arg.number?
      num = arg.value
      int = num.to_i
      Number.with_value(if (num - int).abs == 0.5
                          if int.even?
                            int
                          else
                            int + (int < 0 ? -1 : 1)
                          end
                        else
                          arg.value.round
                        end)
    end


    def self.ceiling_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("ceiling needs a number argument, but received #{arg}", env) unless arg.number?
      Number.with_value(arg.value.ceil)
    end


    def self.floor_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("floor needs a number argument, but received #{arg}", env) unless arg.number?
      Number.with_value(arg.value.floor)
    end


    def self.even_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("even? needs a number argument, but received #{arg}", env) unless arg.number?
      Boolean.with_value(arg.value.even?)
    end


    def self.odd_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("odd? needs a number argument, but received #{arg}", env) unless arg.number?
      Boolean.with_value(arg.value.odd?)
    end


    def self.zero_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("zero? needs a number argument, but received #{arg}", env) unless arg.number?
      Boolean.with_value(arg.value.zero?)
    end


    def self.positive_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("positive? needs a number argument, but received #{arg}", env) unless arg.number?
      Boolean.with_value(arg.value > 0)
    end


    def self.negative_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("negative? needs a number argument, but received #{arg}", env) unless arg.number?
      Boolean.with_value(arg.value < 0)
    end


    def self.interval_impl(args, env)
      initial = args.car
      return Lisp::Debug.process_error("interval needs number arguments, but received #{initial}", env) unless initial.number?
      final = args.cadr
      return Lisp::Debug.process_error("interval needs number arguments, but received #{final}", env) unless final.number?
      return Lisp::Debug.process_error("interval's arguments need to be in natural order", env) unless initial.value <= final.value
      Lisp::ConsCell.array_to_list((initial.value..final.value).to_a.map {|n| Number.with_value(n)})
    end


    def self.random_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("random needs a number argument, but received #{arg}", env) unless arg.nil? || arg.number?
      Number.with_value(arg.nil? ? rand() : rand(arg.value))
    end


    def self.float_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("float needs a numeric or string argument, but received #{arg}", env) unless arg.number? || arg.string?
      Number.with_value(arg.value.to_f)
    end


    def self.integer_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("integer needs a numeric or string argument, but received #{arg}", env) unless arg.number? || arg.string?
      Number.with_value(arg.value.to_i)
    end


    def self.abs_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("abs needs a numeric argument, but received #{arg}", env) unless arg.number?
      Number.with_value(arg.value.abs)
    end


    def self.sqrt_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("sqrt needs a numeric argument, but received #{arg}", env) unless arg.number?
      Number.with_value(::Math.sqrt(arg.value).round(5))
    end


    def self.min_impl(args, env)
      initial = args.car
      return Lisp::Debug.process_error("min requires numeric arguments, but received #{initial}", env) unless initial.number?
      acc = initial.value
      c = args.cdr
      while !c.nil?
        n = c.car
        return Lisp::Debug.process_error("min needs number arguments, but received #{n}", env) unless n.number?
        acc = n.value if n.value < acc
        c = c.cdr
      end

      Number.with_value(acc)
    end


    def self.max_impl(args, env)
      initial = args.car
      return Lisp::Debug.process_error("max requires numeric arguments, but received #{initial}", env) unless initial.number?
      acc = initial.value
      c = args.cdr
      while !c.nil?
        n = c.car
        return Lisp::Debug.process_error("max needs number arguments, but received #{n}", env) unless n.number?
        acc = n.value if n.value > acc
        c = c.cdr
      end

      Number.with_value(acc)
    end


    def self.sin_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("sin needs a numeric argument, but received #{arg}", env) unless arg.number?
      Number.with_value(::Math.sin(arg.value).round(5))
    end


    def self.cos_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("cos needs a numeric argument, but received #{arg}", env) unless arg.number?
      Number.with_value(::Math.cos(arg.value).round(5))
    end


    def self.tan_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("tan needs a numeric argument, but received #{arg}", env) unless arg.number?
      Number.with_value(::Math.tan(arg.value).round(5))
    end


    def self.pred_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("pred needs a numeric argument, but received #{arg}", env) unless arg.number?
      Number.with_value(arg.value - 1)
    end


    def self.succ_impl(args, env)
      arg = args.car
      return Lisp::Debug.process_error("succ needs a numeric argument, but received #{arg}", env) unless arg.number?
      Number.with_value(arg.value + 1)
    end


  end
end
