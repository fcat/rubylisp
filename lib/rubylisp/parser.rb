module Lisp

  class Parser

    def initialize
    end

    def make_number(str)
      Lisp::Number.with_value(str.to_i)
    end

    def make_hex_number(str)
      Lisp::Number.with_value(["0x", str].join.to_i(0))
    end

    def make_float(str)
      Lisp::Number.with_value(str.to_f)
    end

    def make_string(str)
      Lisp::String.with_value(str)
    end

    def make_symbol(str)
      Lisp::Symbol.named(str)
    end

    def make_character(ch)
      Lisp::Character.with_value(ch)
    end

    def parse_cons_cell(tokens)
      tok, lit = tokens.next_token
      if tok == :RPAREN
        tokens.consume_token
        return Lisp::ConsCell.cons()
      end

      car = nil
      cdr = nil
      cells = []
      while tok != :RPAREN
        if tok == :PERIOD
          tokens.consume_token
          cdr = self.parse_sexpr(tokens)
          return nil if tokens.next_token[0] == :EOF
          tok, lit = tokens.next_token
          return Lisp::Debug.process_error("Expected ')' to follow a dotted tail on line #{tokens.line_number}", Lisp::EnvironmentFrame.global) if tok != :RPAREN
          tokens.consume_token
          return Lisp::ConsCell.array_to_list(cells, cdr)
        else
          car = self.parse_sexpr(tokens)
          return Lisp::Debug.process_error("Unexpected EOF (expected ')') on line #{tokens.line_number}", Lisp::EnvironmentFrame.global) if tokens.next_token[0] == :EOF
          cells << car
        end
        tok, lit = tokens.next_token
      end

      tokens.consume_token
      Lisp::ConsCell.array_to_list(cells)
    end

    def parse_frame(tokens, literal)
      m = {}
      tok, lit = tokens.next_token
      if tok == :RBRACE
        tokens.consume_token
        if literal
          Lisp::Frame.new
        else
          Lisp::ConsCell.cons(Lisp::Symbol.named("make-frame"), nil)
        end
      else
        cells = []
        while tok != :RBRACE
          item = self.parse_sexpr(tokens)
          return Lisp::Debug.process_error("Unexpected EOF (expected '}') on line #{tokens.line_number}", env) if tokens.next_token[0] == :EOF
          cells << item
          tok, lit = tokens.next_token
        end
        
        tokens.consume_token
        keys_and_values = Lisp::ConsCell.array_to_list(cells)
        if literal
          Lisp::PrimFrame.make_frame_impl(keys_and_values, Lisp::EnvironmentFrame.global)
        else
          Lisp::ConsCell.cons(Lisp::Symbol.named("make-frame"), keys_and_values)
        end
      end
    end


    def parse_vector(tokens, literal)
      v = []
      tok, lit = tokens.next_token
      if tok == :RPAREN
        tokens.consume_token
        if literal
          Lisp::Vector.new
        else
          Lisp::ConsCell.cons(Lis::Symbol.named("vector"), nil)
        end
      else
        cells = []
        while tok != :RPAREN
          item = self.parse_sexpr(tokens)
          return Lisp::Debug.process_error("Unexpected EOF (expected ')') on line #{tokens.line_number}", env) if tokens.next_token[0] == :EOF
          cells << item
          tok, lit = tokens.next_token
        end
        
        tokens.consume_token
        
        if literal
          Lisp::Vector.with_array(cells)
        else
          Lisp::ConsCell.cons(Lisp::Symbol.named("vector"), Lisp::ConsCell.array_to_list(cells))
        end
      end
    end

    
    def parse_sexpr(tokens)
      while true
        tok, lit = tokens.next_token
        # puts "token: <#{tok}, #{lit}>"
        return nil if tok == :EOF
        tokens.consume_token
        case tok
        when :COMMENT
          next
        when :NUMBER
          return make_number(lit)
        when :FLOAT
          return make_float(lit)
        when :HEXNUMBER
          return make_hex_number(lit)
        when :STRING
          return make_string(lit)
        when :CHARACTER
          return make_character(lit)
        when :LPAREN
          return parse_cons_cell(tokens)
        when :LBRACE
          return parse_frame(tokens, false)
        when :QUOTE_LBRACE
          return parse_frame(tokens, true)
        when :HASH_LPAREN
          return parse_vector(tokens, false)
        when :QUOTE_HASH_LPAREN
          return parse_vector(tokens, true)
        when :SYMBOL
          return make_symbol(lit)
        when :FFI_NEW_SYMBOL
          return Lisp::FfiNew.new(lit)
        when :FFI_SEND_SYMBOL
          return Lisp::FfiSend.new(lit)
        when :FFI_STATIC_SYMBOL
          return Lisp::FfiStatic.new(lit)
        when :FALSE
          return Lisp::FALSE
        when :TRUE
          return Lisp::TRUE
        when :QUOTE
          expr = parse_sexpr(tokens)
          return Lisp::ConsCell.array_to_list([Lisp::Symbol.named('quote'), expr])
        when :BACKQUOTE
          expr = parse_sexpr(tokens)
          return Lisp::ConsCell.array_to_list([Lisp::Symbol.named('quasiquote'), expr])
        when :COMMA
          expr = parse_sexpr(tokens)
          return Lisp::ConsCell.array_to_list([Lisp::Symbol.named('unquote'), expr])
        when :COMMAAT
          expr = parse_sexpr(tokens)
          return Lisp::ConsCell.array_to_list([Lisp::Symbol.named('unquote-splicing'), expr])
        when :WHITESPACE
          next
        when :ILLEGAL
          return Lisp::Debug.process_error("Illegal token: #{lit} on line #{tokens.line_number}", Lisp::EnvironmentFrame.global)
        else
          return make_symbol(lit)
        end
      end
    end

    def parse(src)
      tokenizer = Tokenizer.from_string(src)
      self.parse_sexpr(tokenizer)
    end

    def parse_object_from_file(port, env=Lisp::EnvironmentFrame.global)
      tokenizer = Tokenizer.from_file(port)
      result = self.parse_sexpr(tokenizer)
      result.nil? ? Lisp::EofObject.instance : result
    end
    
    def parse_and_eval(src, env=Lisp::EnvironmentFrame.global)
      sexpr = self.parse(src)
      return sexpr.evaluate(env)
    end

    def parse_and_eval_all(src, env=Lisp::EnvironmentFrame.global)
      tokenizer = Tokenizer.from_string(src)
      result = nil
      until tokenizer.eof?
        sexpr = self.parse_sexpr(tokenizer)
        result = sexpr.evaluate(env)
      end
      result
    end
    
    def process_file(fname)
      File.open(fname) do |f|
        parse_and_eval_all(f.read)
      end
    end
    
  end

end
