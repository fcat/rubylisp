# -*- coding: utf-8 -*-
module Lisp

  class ClassObject < Atom

    def self.new_instance
      self.new(@value.alloc.init)
    end


    def self.with_class(c)
      self.new(c)
    end

    
    def initialize(c)
      @value = c
    end

    
    def with_value(&block)
      block.call(@value)
    end

    
    def class?
      true
    end

    
    def type
      :class
    end

    
    def native_type
      @value.class
    end

    
    def to_s
      "<a class: #{@value.name}>"
    end

    
    def true?
      @value != nil
    end

    
    def false?
      @value == nil
    end

  end
end
