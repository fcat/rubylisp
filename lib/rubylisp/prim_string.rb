module Lisp

  class PrimString

    def self.register
      Primitive.register("str", ">=1")   {|args, env| Lisp::PrimString::str_impl(args, env) }
      Primitive.register("string?", "1")   {|args, env| Lisp::PrimString::stringp_impl(args, env) }
      Primitive.register("make-string", "1|2") {|args, env| Lisp::PrimString::make_string_impl(args, env) }      
      Primitive.register("string", "*") {|args, env| Lisp::PrimString::string_impl(args, env) }      
      Primitive.register("list->string", "1") {|args, env| Lisp::PrimString::list_string_impl(args, env) }      
      Primitive.register("string->list", "1") {|args, env| Lisp::PrimString::string_list_impl(args, env) }      
      Primitive.register("string-copy", "1") {|args, env| Lisp::PrimString::string_copy_impl(args, env) }      
      Primitive.register("string-length", "1") {|args, env| Lisp::PrimString::string_length_impl(args, env) }      
      Primitive.register("string-null?", "1") {|args, env| Lisp::PrimString::string_nullp_impl(args, env) }      
      Primitive.register("string-ref", "2") {|args, env| Lisp::PrimString::string_ref_impl(args, env) }      
      Primitive.register("string-set!", "3") {|args, env| Lisp::PrimString::string_set_impl(args, env) }      

      Primitive.register("string=?", "2") {|args, env| Lisp::PrimString::string_eq_impl(args, env) }      
      Primitive.register("substring=?", "6") {|args, env| Lisp::PrimString::substring_eq_impl(args, env) }      
      Primitive.register("string-ci=?", "2") {|args, env| Lisp::PrimString::string_ci_eq_impl(args, env) }      
      Primitive.register("substring-ci=?", "6") {|args, env| Lisp::PrimString::substring_ci_eq_impl(args, env) }      

      Primitive.register("string<?", "2") {|args, env| Lisp::PrimString::string_lt_impl(args, env) }      
      Primitive.register("substring<?", "6") {|args, env| Lisp::PrimString::substring_lt_impl(args, env) }      
      Primitive.register("string-ci<?", "2") {|args, env| Lisp::PrimString::string_ci_lt_impl(args, env) }      
      Primitive.register("substring-ci<?", "6") {|args, env| Lisp::PrimString::substring_ci_lt_impl(args, env) }      

      Primitive.register("string>?", "2") {|args, env| Lisp::PrimString::string_gt_impl(args, env) }      
      Primitive.register("substring>?", "6") {|args, env| Lisp::PrimString::substring_gt_impl(args, env) }      
      Primitive.register("string-ci>?", "2") {|args, env| Lisp::PrimString::string_ci_gt_impl(args, env) }      
      Primitive.register("substring-ci>?", "6") {|args, env| Lisp::PrimString::substring_ci_gt_impl(args, env) }      

      Primitive.register("string<=?", "2") {|args, env| Lisp::PrimString::string_lte_impl(args, env) }      
      Primitive.register("substring<=?", "6") {|args, env| Lisp::PrimString::substring_lte_impl(args, env) }      
      Primitive.register("string-ci<=?", "2") {|args, env| Lisp::PrimString::string_ci_lte_impl(args, env) }      
      Primitive.register("substring-ci<=?", "6") {|args, env| Lisp::PrimString::substring_ci_lte_impl(args, env) }      

      Primitive.register("string>=?", "2") {|args, env| Lisp::PrimString::string_gte_impl(args, env) }      
      Primitive.register("substring>=?", "6") {|args, env| Lisp::PrimString::substring_gte_impl(args, env) }      
      Primitive.register("string-ci>=?", "2") {|args, env| Lisp::PrimString::string_ci_gte_impl(args, env) }      
      Primitive.register("substring-ci>=?", "6") {|args, env| Lisp::PrimString::substring_ci_gte_impl(args, env) }      

      Primitive.register("string-compare", "5") {|args, env| Lisp::PrimString::string_compare_impl(args, env) }      
      Primitive.register("string-compare-ci", "5") {|args, env| Lisp::PrimString::string_compare_ci_impl(args, env) }      

      Primitive.register("string-hash", "1") {|args, env| Lisp::PrimString::string_hash_impl(args, env) }      
      Primitive.register("string-hash-mod", "2") {|args, env| Lisp::PrimString::string_hash_mod_impl(args, env) }      

      Primitive.register("string-capitalized?", "1") {|args, env| Lisp::PrimString::string_capitalizedp_impl(args, env) }      
      Primitive.register("substring-capitalized?", "3") {|args, env| Lisp::PrimString::substring_capitalizedp_impl(args, env) }      
      Primitive.register("string-upper-case?", "1") {|args, env| Lisp::PrimString::string_upperp_impl(args, env) }      
      Primitive.register("substring-upper-case?", "3") {|args, env| Lisp::PrimString::substring_upperp_impl(args, env) }      
      Primitive.register("string-lower-case?", "1") {|args, env| Lisp::PrimString::string_lowerp_impl(args, env) }      
      Primitive.register("substring-lower-case?", "3") {|args, env| Lisp::PrimString::substring_lowerp_impl(args, env) }      

      Primitive.register("string-capitalize", "1") {|args, env| Lisp::PrimString::string_capitalize_impl(args, env) }      
      Primitive.register("string-capitalize!", "1") {|args, env| Lisp::PrimString::string_capitalize_bang_impl(args, env) }      
      Primitive.register("substring-capitalize!", "3") {|args, env| Lisp::PrimString::substring_capitalize_bang_impl(args, env) }      
      Primitive.register("string-downcase", "1") {|args, env| Lisp::PrimString::string_downcase_impl(args, env) }      
      Primitive.register("string-downcase!", "1") {|args, env| Lisp::PrimString::string_downcase_bang_impl(args, env) }      
      Primitive.register("substring-downcase!", "3") {|args, env| Lisp::PrimString::substring_downcase_bang_impl(args, env) }      
      Primitive.register("string-upcase", "1") {|args, env| Lisp::PrimString::string_upcase_impl(args, env) }      
      Primitive.register("string-upcase!", "1") {|args, env| Lisp::PrimString::string_upcase_bang_impl(args, env) }      
      Primitive.register("substring-upcase!", "3") {|args, env| Lisp::PrimString::substring_upcase_bang_impl(args, env) }

      Primitive.register("string-append", "*") {|args, env| Lisp::PrimString::string_append_impl(args, env) }      
      Primitive.register("substring", "3") {|args, env| Lisp::PrimString::substring_impl(args, env) }      
      Primitive.register("string-head", "2") {|args, env| Lisp::PrimString::string_head_impl(args, env) }      
      Primitive.register("string-tail", "2") {|args, env| Lisp::PrimString::string_tail_impl(args, env) }      

      Primitive.register("string-pad-left", "2|3") {|args, env| Lisp::PrimString::string_pad_left_impl(args, env) }      
      Primitive.register("string-pad-right", "2|3") {|args, env| Lisp::PrimString::string_pad_right_impl(args, env) }      

      Primitive.register("string-trim", "1|2") {|args, env| Lisp::PrimString::string_trim_impl(args, env) }      
      Primitive.register("string-trim-right", "1|2") {|args, env| Lisp::PrimString::string_trim_right_impl(args, env) }      
      Primitive.register("string-trim-left", "1|2") {|args, env| Lisp::PrimString::string_trim_left_impl(args, env) }      

      Primitive.register("string-split", "2") {|args, env| Lisp::PrimString::string_split_impl(args, env) }      
end

    def self.str_impl(args, env)
      strings = args.to_a.map {|e| e.to_s}
      String.with_value(strings.join)
    end
    

    def self.stringp_impl(args, env)
      return Lisp::Boolean.with_value(args.car.string?)
    end


    def self.make_string_impl(args, env)
      k_arg = args.car
      return Lisp::Debug.process_error("make-string requires an integer as it's first argument.", env) unless k_arg.integer?
      k = k_arg.value
      c = if args.length == 2
            c_arg = args.cadr
            return Lisp::Debug.process_error("make-string requires a character as it's second argument, but received #{c_arg}.", env) unless c_arg.character?
            c_arg.value
          else
            " "
          end
      Lisp::String.with_value(c * k)
    end


    def self.string_impl(args, env)
      chars = args.to_a.map do |a|
        return Lisp::Debug.process_error("string requires character args, but was passed #{a}.", env) unless a.character?
        a.value
      end
      Lisp::String.with_value(chars.join)
    end


    def self.list_string_impl(args, env)
      list_of_chars = args.car
      return Lisp::Debug.process_error("list->string requires a list argument, but received #{list_of_chars}", env) unless list_of_chars.list?      
      chars = list_of_chars.to_a.map do |a|
        ea = a.evaluate(env)
        return Lisp::Debug.process_error("string requires a list of characters, but it contained #{ea}.", env) unless ea.character?
        ea.value
      end
      Lisp::String.with_value(chars.join)
    end


    def self.string_list_impl(args, env)
      str_arg = args.car
      return Lisp::Debug.process_error("string->list requires a string argument, but received #{str_arg}", env) unless str_arg.string?
      chars = str_arg.value.each_char.map {|c| Lisp::PrimCharacter.find_character_for_chr(c) }
      Lisp::ConsCell.array_to_list(chars)
    end


    def self.string_copy_impl(args, env)
      str_arg = args.car
      return Lisp::Debug.process_error("string-copy requires a string argument, but received #{str_arg}", env) unless str_arg.string?
      Lisp::String.with_value(str_arg.value)
    end


    def self.string_length_impl(args, env)
      str_arg = args.car
      return Lisp::Debug.process_error("string-length requires a string argument, but received #{str_arg}", env) unless str_arg.string?
      Lisp::Number.with_value(str_arg.value.length)
    end


    def self.string_nullp_impl(args, env)
      str_arg = args.car
      return Lisp::Debug.process_error("string-length requires a string argument, but received #{str_arg}", env) unless str_arg.string?
      Lisp::Boolean.with_value(str_arg.value.length == 0)
    end


    def self.string_ref_impl(args, env)
      str_arg = args.car
      return Lisp::Debug.process_error("string-ref requires a string as it's first argument, but received #{arg.car}", env) unless str_arg.string?
      str = str_arg.value
      k_arg = args.cadr
      return Lisp::Debug.process_error("string-ref requires it's second argument to be an integer, but received #{k_arg}", env) unless k_arg.integer?
      k = k_arg.value
      return Lisp::FALSE if k < 0 || k >= str.length
      Lisp::PrimCharacter.find_character_for_chr(str[k])
    end


    def self.string_set_impl(args, env)
      str_arg = args.car
      return Lisp::Debug.process_error("string-set! needs a string as it's first argument, but received #{str_arg}", env) unless str_arg.string?
      str = str_arg.value
      k_arg = args.cadr
      return Lisp::Debug.process_error("string-set! requires an integer as it's second argument, but received #{k_arg}", env) unless k_arg.integer?
      k = k_arg.value
      return Lisp::FALSE if k < 0 || k >= str.length
      replacement_arg = args.caddr
      return Lisp::Debug.process_error("string-set! requires a character as it's third argument, but received #{replacement_arg}", env) unless replacement_arg.character?
      replacement = replacement_arg.value
      str[k] = replacement
      Lisp::String.with_value(str)
    end


    def self.get_substring(func, str, start_index, end_index, env)
      return Lisp::Debug.process_error("#{func} requires a string, but received #{str}", env) unless str.string?
      s = str.value
      return Lisp::Debug.process_error("#{func} requires an integer start index, but received #{start_index}", env) unless start_index.integer?
      si = start_index.value
      return Lisp::Debug.process_error("#{func} received an invalid substring start index: #{si}", env) if si < 0 || si > s.length
      return Lisp::Debug.process_error("#{func} requires an integer end index, but received #{end_index}", env) unless end_index.integer?
      ei = end_index.value
      return Lisp::Debug.process_error("#{func} received an invalid substring end index: #{ei}", env) if ei < 0 || ei > s.length
      s[si...ei]
    end


    def self.extract_substrings(func, args, env)
      substr1 = get_substring(func, args.nth(0), args.nth(1), args.nth(2), env)
      substr2 = get_substring(func, args.nth(3), args.nth(4), args.nth(5), env)
      return [substr1, substr2]
    end

    
    def self.get_string(func, str, env)
      return Lisp::Debug.process_error("#{func} requires a string, but received #{str}", env) unless str.string?
      str.value
    end


    def self.extract_strings(func, args, env)
      str1 = get_string(func, args.nth(0).evaluate(env), env)
      str2 = get_string(func, args.nth(1).evaluate(env), env)
      return [str1, str2]
    end

    
    def self.string_eq_impl(args, env)
      str1, str2 = extract_strings("string=?", args, env)
      Lisp::Boolean.with_value(str1 == str2)
    end


    def self.substring_eq_impl(args, env)
      substr1, substr2 = extract_substrings("substring=?", args, env)
      Lisp::Boolean.with_value(substr1 == substr2)
    end


    def self.string_ci_eq_impl(args, env)
      str1, str2 = extract_strings("string-ci=?", args, env)
      Lisp::Boolean.with_value(str1.downcase == str2.downcase)
    end


    def self.substring_ci_eq_impl(args, env)
      substr1, substr2 = extract_substrings("substring-ci=?", args, env)
      Lisp::Boolean.with_value(substr1.downcase == substr2.downcase)
    end


    def self.string_lt_impl(args, env)
      str1, str2 = extract_strings("string<?", args, env)
      Lisp::Boolean.with_value(str1 < str2)
    end


    def self.substring_lt_impl(args, env)
      substr1, substr2 = extract_substrings("substring<?", args, env)
      Lisp::Boolean.with_value(substr1 < substr2)
    end


    def self.string_ci_lt_impl(args, env)
      str1, str2 = extract_strings("string-ci<?", args, env)
      Lisp::Boolean.with_value(str1.downcase < str2.downcase)
    end


    def self.substring_ci_lt_impl(args, env)
      substr1, substr2 = extract_substrings("substring-ci<?", args, env)
      Lisp::Boolean.with_value(substr1.downcase < substr2.downcase)
    end

    
    def self.string_gt_impl(args, env)
      str1, str2 = extract_strings("string>?", args, env)
      Lisp::Boolean.with_value(str1 > str2)
    end


    def self.substring_gt_impl(args, env)
      substr1, substr2 = extract_substrings("substring>?", args, env)
      Lisp::Boolean.with_value(substr1 > substr2)
    end


    def self.string_ci_gt_impl(args, env)
      str1, str2 = extract_strings("string-ci>?", args, env)
      Lisp::Boolean.with_value(str1.downcase > str2.downcase)
    end


    def self.substring_ci_gt_impl(args, env)
      substr1, substr2 = extract_substrings("substring-ci>?", args, env)
      Lisp::Boolean.with_value(substr1.downcase > substr2.downcase)
    end

    
    def self.string_lte_impl(args, env)
      str1, str2 = extract_strings("string<=?", args, env)
      Lisp::Boolean.with_value(str1 <= str2)
    end


    def self.substring_lte_impl(args, env)
      substr1, substr2 = extract_substrings("substring<=?", args, env)
      Lisp::Boolean.with_value(substr1 <= substr2)
    end


    def self.string_ci_lte_impl(args, env)
      str1, str2 = extract_strings("string-ci<=?", args, env)
      Lisp::Boolean.with_value(str1.downcase <= str2.downcase)
    end


    def self.substring_ci_lte_impl(args, env)
      substr1, substr2 = extract_substrings("substring-ci<=?", args, env)
      Lisp::Boolean.with_value(substr1.downcase <= substr2.downcase)
    end

    
    def self.string_gte_impl(args, env)
      str1, str2 = extract_strings("string>=?", args, env)
      Lisp::Boolean.with_value(str1 >= str2)
    end


    def self.substring_gte_impl(args, env)
      substr1, substr2 = extract_substrings("substring>=?", args, env)
      Lisp::Boolean.with_value(substr1 >= substr2)
    end


    def self.string_ci_gte_impl(args, env)
      str1, str2 = extract_strings("string-ci>=?", args, env)
      Lisp::Boolean.with_value(str1.downcase >= str2.downcase)
    end


    def self.substring_ci_gte_impl(args, env)
      substr1, substr2 = extract_substrings("substring-ci>=?", args, env)
      Lisp::Boolean.with_value(substr1.downcase >= substr2.downcase)
    end

    
    def self.string_compare_impl(args, env)
      str1 = get_string("string-compare", args.nth(0), env)
      str2 = get_string("string-compare", args.nth(1), env)
      f_number = case str1 <=> str2
                 when -1
                   3
                 when 0
                   2
                 when 1
                   4
                 end
      f = args.nth(f_number)
      return Lisp::Debug.process_error("string-compare requires functions for argument #{f_number}, but received #{f}", env) unless f.function?
      f.apply_to(Lisp::ConsCell.cons, env)
    end


    def self.string_compare_ci_impl(args, env)
      str1 = get_string("string-compare-ci", args.nth(0), env)
      str2 = get_string("string-compare-ci", args.nth(1), env)
      f_number = case str1.downcase <=> str2.downcase
                 when -1
                   3
                 when 0
                   2
                 when 1
                   4
                 end
      f = args.nth(f_number)
      return Lisp::Debug.process_error("string-compare-ci requires functions for argument #{f_number}, but received #{f}", env) unless f.function?
      f.apply_to(Lisp::ConsCell.cons, env)
    end


    def self.string_hash_impl(args, env)
      str = get_string("string-hash", args.nth(0), env)
      Lisp::Number.with_value(str.hash)
    end


    def self.string_hash_mod_impl(args, env)
      str = get_string("string-hash-mod", args.nth(0), env)
      k_arg = args.cadr
      return Lisp::Debug.process_error("string-hash-mod requires it's second argument to be an integer, but received #{k_arg}", env) unless k_arg.integer?
      k = k_arg.value
      Lisp::Number.with_value(str.hash % k)
    end


    # str is assumed to be a single word
    def self.uppercase?(str)
      (str =~ /^[[:upper:]]*$/) == 0
    end
    

    def self.lowercase?(str)
      (str =~ /^[[:lower:]]*$/) == 0
    end
    

    def self.capitalized?(str)
      first = str[0]
      rest = str[1..-1]
      return false unless first =~ /[[:upper:]]/
      lowercase?(rest)
    end


    def self.split_into_words(str)
      str.split(/[^[[:alpha:]]]+/)
    end
    

    def self.string_capitalizedp_impl(args, env)
      str = get_string("string-capitalized?", args.nth(0), env)
      words = split_into_words(str)
      Lisp::Boolean.with_value(capitalized?(words[0])&& words[1..-1].all? {|w| capitalized?(w) || lowercase?(w)})
    end


    def self.substring_capitalizedp_impl(args, env)
      str = get_substring("substring-capitalized?", args.nth(0), args.nth(1), args.nth(2), env)
      words = split_into_words(str)
      Lisp::Boolean.with_value(capitalized?(words[0]) && words[1..-1].all? {|w| capitalized?(w) || lowercase?(w)})
    end


    def self.string_upperp_impl(args, env)
      str = get_string("string-upper-case?", args.nth(0), env)
      words = split_into_words(str)
      Lisp::Boolean.with_value(words.all? {|w| uppercase?(w)})
    end


    def self.substring_upperp_impl(args, env)
      str = get_substring("substring-upper-case?", args.nth(0), args.nth(1), args.nth(2), env)
      words = split_into_words(str)
      Lisp::Boolean.with_value(words.all? {|w| uppercase?(w)})
    end


    def self.string_lowerp_impl(args, env)
      str = get_string("string-lower-case?", args.nth(0), env)
      words = split_into_words(str)
      Lisp::Boolean.with_value(words.all? {|w| lowercase?(w)})
    end


    def self.substring_lowerp_impl(args, env)
      str = get_substring("substring-lower-case?", args.nth(0), args.nth(1), args.nth(2), env)
      words = split_into_words(str)
      Lisp::Boolean.with_value(words.all? {|w| lowercase?(w)})
    end


    def self.capitalize_string(str)
      saw_first = false
      str.chars.map do |c|
        if c =~ /[[:alpha:]]/
          if saw_first
            c.downcase
          else
            saw_first = true
            c.upcase
          end
        else
          c
        end
      end
    end

    def self.string_capitalize_impl(args, env)
      str = get_string("string-capitalize", args.nth(0), env)
      new_chars = capitalize_string(str)
      new_str = ""
      new_chars.each {|c| new_str << c}
      Lisp::String.with_value(new_str)
    end


    def self.string_capitalize_bang_impl(args, env)
      str = args.nth(0)
      return Lisp::Debug.process_error("string-capitalize! requires a string, but received #{str}", env) unless str.string?
      new_chars = capitalize_string(str.value)
      new_str = ""
      new_chars.each {|c| new_str << c}
      str.set!(new_str)
      str
    end


    def self.substring_capitalize_bang_impl(args, env)
      s = args.nth(0)
      return Lisp::Debug.process_error("substring-capitalize! requires a string as it's first argument, but received #{s}", env) unless s.string?
      str = s.value
      
      start_index = args.nth(1)
      return Lisp::Debug.process_error("substring-capitalize! requires an integer start index, but received #{start_index}", env) unless start_index.integer?
      si = start_index.value
      return Lisp::Debug.process_error("substring-capitalize! received an invalid substring start index: #{si}", env) if si < 0 || si > str.length

      end_index = args.nth(2)
      return Lisp::Debug.process_error("substring-capitalize! requires an integer end index, but received #{end_index}", env) unless end_index.integer?
      ei = end_index.value
      return Lisp::Debug.process_error("substring-capitalize! received an invalid substring end index: #{ei}", env) if ei < 0 || ei > str.length

      prefix = str[0...si]
      substr = str[si...ei]
      suffix = str[ei..-1]
      
      new_chars = capitalize_string(substr)
      new_substr = ""   
      new_chars.each {|c| new_substr << c}
      s.set!(prefix + new_substr + suffix)
      s
    end
    

    def self.string_downcase_impl(args, env)
      str = get_string("string-downcase?", args.nth(0), env)
      Lisp::String.with_value(str.downcase)
    end


    def self.string_downcase_bang_impl(args, env)
      str = args.nth(0)
      return Lisp::Debug.process_error("string-downcase! requires a string, but received #{str}", env) unless str.string?
      str.set!(str.value.downcase)
      str
    end


    def self.substring_downcase_bang_impl(args, env)
      s = args.nth(0)
      return Lisp::Debug.process_error("substring-downcase! requires a string as it's first argument, but received #{s}", env) unless s.string?
      str = s.value
      
      start_index = args.nth(1)
      return Lisp::Debug.process_error("substring-downcase! requires an integer start index, but received #{start_index}", env) unless start_index.integer?
      si = start_index.value
      return Lisp::Debug.process_error("substring-downcase! received an invalid substring start index: #{si}", env) if si < 0 || si > str.length

      end_index = args.nth(2)
      return Lisp::Debug.process_error("substring-downcase! requires an integer end index, but received #{end_index}", env) unless end_index.integer?
      ei = end_index.value
      return Lisp::Debug.process_error("substring-downcase! received an invalid substring end index: #{ei}", env) if ei < 0 || ei > str.length

      prefix = str[0...si]
      substr = str[si...ei]
      suffix = str[ei..-1]
      
      new_chars = capitalize_string(substr)
      new_substr = ""   
      new_chars.each {|c| new_substr << c}
      s.set!(prefix + substr.downcase + suffix)
      s
    end
    

    def self.string_upcase_impl(args, env)
      str = get_string("string-upcase?", args.nth(0), env)
      Lisp::String.with_value(str.upcase)
    end


    def self.string_upcase_bang_impl(args, env)
      str = args.nth(0)
      return Lisp::Debug.process_error("string-upcase! requires a string, but received #{str}", env) unless str.string?
      str.set!(str.value.upcase)
      str
    end


    def self.substring_upcase_bang_impl(args, env)
      s = args.nth(0)
      return Lisp::Debug.process_error("substring-upcase! requires a string as it's first argument, but received #{s}", env) unless s.string?
      str = s.value
      
      start_index = args.nth(1)
      return Lisp::Debug.process_error("substring-upcase! requires an integer start index, but received #{start_index}", env) unless start_index.integer?
      si = start_index.value
      return Lisp::Debug.process_error("substring-upcase! received an invalid substring start index: #{si}", env) if si < 0 || si > str.length

      end_index = args.nth(2)
      return Lisp::Debug.process_error("substring-upcase! requires an integer end index, but received #{end_index}", env) unless end_index.integer?
      ei = end_index.value
      return Lisp::Debug.process_error("substring-upcase! received an invalid substring end index: #{ei}", env) if ei < 0 || ei > str.length

      prefix = str[0...si]
      substr = str[si...ei]
      suffix = str[ei..-1]
      
      new_chars = capitalize_string(substr)
      new_substr = ""   
      new_chars.each {|c| new_substr << c}
      s.set!(prefix + substr.upcase + suffix)
      s
    end


    def self.string_append_impl(args, env)
      strings = args.to_a.map do |a|
        return Lisp::Debug.process_error("string-append requires strings, but received #{a}", env) unless a.string?
        a.value
      end

      Lisp::String.with_value(strings.join)
    end


    def self.substring_impl(args, env)
      str = get_substring("substring", args.nth(0), args.nth(1), args.nth(2), env)
      Lisp::String.with_value(str)
    end


    def self.string_head_impl(args, env)
      s = args.nth(0)
      return Lisp::Debug.process_error("string-head requires a string as it's first argument, but received #{s}", env) unless s.string?
      str = s.value
      
      end_index = args.nth(1)
      return Lisp::Debug.process_error("string-head requires an integer end index, but received #{end_index}", env) unless end_index.integer?
      ei = end_index.value
      return Lisp::Debug.process_error("string-head received an invalid end index: #{ei}", env) if ei < 0 || ei > str.length

      Lisp::String.with_value(str[0...ei])
    end


    def self.string_tail_impl(args, env)
      return Lisp::Debug.process_error("string-tail requires 2 arguments, but received #{args.length}", env) unless args.length == 2
      s = args.nth(0).evaluate(env)
      return Lisp::Debug.process_error("string-tail requires a string as it's first argument, but received #{s}", env) unless s.string?
      str = s.value
      
      start_index = args.nth(1).evaluate(env)
      return Lisp::Debug.process_error("string-tail requires an integer start index, but received #{start_index}", env) unless start_index.integer?
      si = start_index.value
      return Lisp::Debug.process_error("string-tail received an invalid end index: #{si}", env) if si < 0 || si > str.length

      Lisp::String.with_value(str[si..-1])
    end


    def self.string_pad_left_impl(args, env)
      s = args.nth(0)
      return Lisp::Debug.process_error("string-pad-left requires a string as it's first argument, but received #{s}", env) unless s.string?
      str = s.value
      
      size_arg = args.nth(1)
      return Lisp::Debug.process_error("string-pad-left requires an integer size, but received #{size_arg}", env) unless size_arg.integer?
      size = size_arg.value
      return Lisp::Debug.process_error("string-pad-left received an invalid size: #{size}", env) if size < 0

      padding_char = if args.length == 3
                       ch_arg = args.nth(2)
                       return Lisp::Debug.process_error("string-pad-left requires a character pad, but received #{ch_arg}", env) unless ch_arg.character?
                       ch_arg.value
                     else
                       " "
                     end
        
      
      new_str = if size > str.length
                  padding = size - str.length
                  pad = ""
                  padding.times {|i| pad << padding_char}
                  pad + str
                else
                  start = str.length - size
                  str[start..-1]
                end
      Lisp::String.with_value(new_str)
    end


    def self.string_pad_right_impl(args, env)
      s = args.nth(0)
      raise "string-pad-right requires a string as it's first argument, but received #{s}" unless s.string?
      str = s.value
      
      size_arg = args.nth(1)
      raise "string-pad-right requires an integer size, but received #{size_arg}" unless size_arg.integer?
      size = size_arg.value
      raise "string-pad-right received an invalid size: #{size}" if size < 0

      padding_char = if args.length == 3
                       ch_arg = args.nth(2)
                       raise "string-pad-right requires a character pad, but received #{ch_arg}" unless ch_arg.character?
                       ch_arg.value
                     else
                       " "
                     end
        
      
      new_str = if size > str.length
                  padding = size - str.length
                  pad = ""
                  padding.times {|i| pad << padding_char}
                  str + pad
                else
                  last = str.length - size
                  str[0...4]
                end
      Lisp::String.with_value(new_str)
    end


    def self.string_trim_impl(args, env)
      s1 = args.nth(0)
      raise "string-trim requires a string as it's first argument, but received #{s1}" unless s1.string?
      str = s1.value

      pattern = Regexp.new(if args.length == 2
                             s2 = args.nth(1)
                             raise "string-trim requires a string as it's second argument, but received #{s2}" unless s2.string?
                             s2.value
                           else
                             "[[:graph:]]"
                           end)

      
      left_i = 0
      while pattern.match(str[left_i]).nil? && left_i < str.length
        left_i += 1
      end
      
      right_i = str.length - 1
      while pattern.match(str[right_i]).nil? && right_i >= 0
        right_i -= 1
      end
      
      Lisp::String.with_value(str[left_i..right_i])
    end


    def self.string_trim_left_impl(args, env)
      s1 = args.nth(0)
      raise "string-trim-left requires a string as it's first argument, but received #{s1}" unless s1.string?
      str = s1.value

      pattern = Regexp.new(if args.length == 2
                             s2 = args.nth(1)
                             raise "string-trim-left requires a string as it's second argument, but received #{s2}" unless s2.string?
                             s2.value
                           else
                             "[[:graph:]]"
                           end)

      
      left_i = 0
      while pattern.match(str[left_i]).nil? && left_i < str.length
        left_i += 1
      end
      
      Lisp::String.with_value(str[left_i..-1])
    end


    def self.string_trim_right_impl(args, env)
      s1 = args.nth(0)
      raise "string-trim-right requires a string as it's first argument, but received #{s1}" unless s1.string?
      str = s1.value

      pattern = Regexp.new(if args.length == 2
                             s2 = args.nth(1)
                             raise "string-trim-right requires a string as it's second argument, but received #{s2}" unless s2.string?
                             s2.value
                           else
                             "[[:graph:]]"
                           end)

      right_i = str.length - 1
      while pattern.match(str[right_i]).nil? && right_i >= 0
        right_i -= 1
      end
      
      Lisp::String.with_value(str[0..right_i])
    end

    
    def self.string_split_impl(args, env)
      the_string = args.car
      raise "string-split requires a string as it's first argument, but received #{the_string}" unless the_string.string?
      
      separator = args.cadr
      raise "string-split requires a string as it's second argument, but received #{separator}" unless separator.string?

      Lisp::ConsCell.array_to_list(the_string.value.split(separator.value).map {|s| Lisp::String.with_value(s)})
    end

  end
  
end
