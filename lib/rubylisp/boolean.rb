module Lisp

  class Boolean < Atom
    def self.TRUE
      @true_constant ||= Lisp::Boolean.new(true)
    end

    def self.FALSE
      @false_constant ||= Lisp::Boolean.new(false)
    end

    def self.with_value(b)
      b ? self.TRUE : self.FALSE
    end

    def initialize(b)
      @value = b
    end

    def type
      :boolean
    end

    def boolean?
      true
    end

    def to_s
      if @value
        "#t"
      else
        "#f"
      end
    end

    def true?
      @value
    end

    def false?
      !@value
    end

    def negate
      Lisp::Boolean.with_value(!@value)
    end
    
  end

  TRUE = Lisp::Boolean.TRUE
  FALSE = Lisp::Boolean.FALSE
end
