module Lisp

  class PrimTypeChecks

    def self.register
      Primitive.register("list?", "1")     {|args, env| Lisp::PrimTypeChecks::typep_impl(args.car, :pair) }
      Primitive.register("pair?", "1")     {|args, env| Lisp::PrimTypeChecks::typep_impl(args.car, :pair) }
      Primitive.register("symbol?", "1")   {|args, env| Lisp::PrimTypeChecks::typep_impl(args.car, :symbol) }
      Primitive.register("number?", "1")   {|args, env| Lisp::PrimTypeChecks::typep_impl(args.car, :number) }
      Primitive.register("frame?", "1")   {|args, env| Lisp::PrimTypeChecks::typep_impl(args.car, :frame) }

      Primitive.register("integer?", "1")  {|args, env| Lisp::PrimTypeChecks::integerp_impl(args, env) }
      Primitive.register("float?", "1")    {|args, env| Lisp::PrimTypeChecks::floatp_impl(args, env) }
      Primitive.register("function?", "1") {|args, env| Lisp::PrimTypeChecks::functionp_impl(args, env) }

      Primitive.register("nil?", "1")      {|args, env| Lisp::PrimTypeChecks::nilp_impl(args, env) }
      Primitive.register("null?", "1")     {|args, env| Lisp::PrimTypeChecks::nilp_impl(args, env) }
      Primitive.register("not-nil?", "1")  {|args, env| Lisp::PrimTypeChecks::not_nilp_impl(args, env) }
      Primitive.register("not-null?", "1") {|args, env| Lisp::PrimTypeChecks::not_nilp_impl(args, env) }
    end


    def self.typep_impl(val, sym)
      return Lisp::Boolean.with_value(val.type == sym)
    end


    def self.integerp_impl(args, env)
      val = args.car
      return Lisp::Boolean.with_value(val.type == :number && val.integer?)
    end


    def self.floatp_impl(args, env)
      val = args.car
      return Lisp::Boolean.with_value(val.type == :number && val.float?)
    end


    def self.functionp_impl(args, env)
      val = args.car
      return Lisp::Boolean.with_value(val.type == :function || val.type == :primitive)
    end


    def self.nilp_impl(args, env)
      return Lisp::Boolean.with_value(args.car.nil? || (args.car.pair? && args.car.empty?))
    end


    def self.not_nilp_impl(args, env)
      return Lisp::FALSE if args.car.nil?
      return Lisp::TRUE unless args.car.pair?
      return Lisp::Boolean.with_value(args.car.pair? && !args.car.empty?)
    end

  end
end
