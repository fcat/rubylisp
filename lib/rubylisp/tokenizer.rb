module Lisp

  class Tokenizer

    attr_reader :line_number

    def self.from_string(str, absorb_space=true)
      self.new(StringIO.new(str, "r"), absorb_space)
    end

    def self.from_file(f)
      self.new(f)
    end
    
    def initialize(src, absorb_space=true)
      @absorb_whitespace = absorb_space
      @source = src
      @lookahead_token = nil
      @lookahead_literal = ''
      @eof = false
      @almost_eof = false
      @curent_ch = nil
      @next_ch = nil
      @next_next_ch = nil
      init
    end


    def advance
      if @source.eof?
        @eof = true
        @almost_eof = true
        @current_ch = nil
        @next_ch = nil
      else
        @current_ch = @source.readchar
        if @source.eof?
          @almost_eof = true
          @next_ch = nil
        else
          @next_ch = @source.readchar
          if @source.eof?
            @next_next_ch = nil
          else
            @next_next_ch = @source.readchar
            @source.ungetc(@next_next_ch)
          end
          @source.ungetc(@next_ch)
        end
      end
    end
    
    def next_token
      return @lookahead_token, @lookahead_literal
    end

    def eof?
      @eof
    end

    def almost_eof?
      @almost_eof
    end

    def letter?(ch)
      ch =~ /[[:alpha:]]/
    end

    def hex?(ch)
      ch =~ /[abcdefABCDEF]/
    end

    def digit?(ch)
      ch =~ /[[:digit:]]/
    end

    def number?(ch)
      digit?(ch) || (ch == '-' && digit?(@next_ch))
    end


    def space?(ch)
      ch =~ /[[:space:]]/
    end


    def graph?(ch)
      ch =~ /[[:graph:]]/
    end


    def symbol_character?(ch)
      graph?(ch) && !("();\"'`|[]{}#,.".include?(ch))
    end

    def read_symbol
      lit = ""
      tok = nil
      if @current_ch == '.'
        lit << @current_ch
        advance
        tok = :FFI_SEND_SYMBOL
      end

      while !eof? && ((@current_ch == '.' && !symbol_character?(@next_ch) && tok.nil?) ||
                      (@current_ch == '/' && symbol_character?(@next_ch)) ||
                      (symbol_character?(@current_ch)))
        tok ||= :FFI_NEW_SYMBOL if @current_ch == '.' && !lit.empty?
        tok = :FFI_STATIC_SYMBOL if @current_ch == '/' && !lit.empty?
        lit << @current_ch
        advance
      end

      tok ||= :SYMBOL
      return tok, case tok
                  when :SYMBOL, :FFI_STATIC_SYMBOL
                    lit
                  when :FFI_SEND_SYMBOL
                    lit[1..-1]
                  when :FFI_NEW_SYMBOL
                    lit[0..-2]
                  end
    end

    def read_number(hex)
      lit = ""
      if @current_ch == '-'
        lit << @current_ch
        advance
      end
      is_float = false
      while !eof? && (digit?(@current_ch) || (hex && hex?(@current_ch)) || (!hex && !is_float && @current_ch == '.'))
        is_float ||= !hex && (@current_ch == '.')
        lit << @current_ch
        advance
      end

      tok = if hex
              :HEXNUMBER
            elsif is_float
              :FLOAT
            else
              :NUMBER
            end

      return tok, lit
    end


    def process_escapes(str)
      i = 0
      processed_str = ""
      while i < str.length
        if str[i] == '\\'
          processed_str << if i < (str.length - 1)
                             i += 1
                             case (str[i])
                             when 'n'
                               "\n"
                             when 't'
                               "\t"
                             when '\\'
                               "\\"
                             else
                               "\\#{str[i]}"
                             end
                           else
                             "\\"
                           end
        else
          processed_str << str[i]
        end
        i += 1
      end
      processed_str
    end


    def read_string
      lit = ""
      while !eof? && @current_ch != '"'
        lit << @current_ch
        advance
      end

      return :EOF, '' if eof?
      advance
      return :STRING, process_escapes(lit)
    end


    def divider?(ch)
      ch =~ /[[[:space:]]\(\)\{\}<>\[\]]/
    end


    def read_character
      lit = ""
      lit << @current_ch
      advance
      while !eof? && !divider?(@current_ch)
        lit << @current_ch
        advance
      end

      return :CHARACTER, lit
    end


    def read_next_token
      return :EOF, '' if eof?

      if @absorb_whitespace
        while space?(@current_ch)
          @line_number += 1 if @current_ch == "\n"
          advance
          return :EOF, '' if eof?
        end
      end
      
      if !@absorb_whitespace && space?(@current_ch)
        @line_number += 1 if @current_ch == "\n"
        advance
        return :WHITESPACE, " "
      elsif number?(@current_ch)
        return read_number(false)
      elsif @current_ch == '-' && number?(@next_ch)
        return read_number(false)
      elsif @current_ch == '#' && @next_ch == 'x'
        advance
        advance
        return read_number(true)
      elsif @current_ch == '"'
        advance
        return read_string
      elsif @current_ch == '#' && @next_ch == '\\'
        advance
        advance
        return read_character
      elsif @current_ch == '\'' && @next_ch == '{'
        advance
        advance
        return :QUOTE_LBRACE, "'{"
      elsif @current_ch == '\'' && @next_ch == '#' && @next_next_ch == '('
        advance
        advance
        advance
        return :QUOTE_HASH_LPAREN, "'#("
      elsif @current_ch == '\''
        advance
        return :QUOTE, "'"
      elsif @current_ch == '`'
        advance
        return :BACKQUOTE, "`"
      elsif @current_ch == ',' && @next_ch == '@'
        advance
        advance
        return :COMMAAT, ",@"
      elsif @current_ch == ','
        advance
        return :COMMA, ","
      elsif @current_ch == '('
        advance
        return :LPAREN, "("
      elsif @current_ch == '#' && @next_ch == '('
        advance
        advance
        return :HASH_LPAREN, "#("
      elsif @current_ch == ')'
        advance
        return :RPAREN, ")"
      elsif @current_ch == '{'
        advance
        return :LBRACE, "{"
      elsif @current_ch == '}'
        advance
        return :RBRACE, "}"
      elsif @current_ch == '['
        advance
        return :LBRACKET, "["
      elsif @current_ch == ']'
        advance
        return :RBRACKET, "]"
      elsif @current_ch == '.' && space?(@next_ch)
        advance
        return :PERIOD, "."
      elsif @current_ch == '.' && symbol_character?(@next_ch)
        return read_symbol
        elsif symbol_character?(@current_ch)
          return read_symbol
        elsif @current_ch == '#' && @next_ch == 't'
        advance
        advance
        return :TRUE, "#t"
      elsif @current_ch == '#' && @next_ch == 'f'
        advance
        advance
        return :FALSE, "#f"
      elsif @current_ch == ';'
        lit = ""
        while true
          return :COMMENT, lit if eof? || @current_ch == "\n"
          lit << @current_ch
          advance
        end
      else
        return :ILLEGAL, @current_ch
      end
    end

    def consume_token
      @lookahead_token, @lookahead_literal = self.read_next_token
      consume_token if @lookahead_token == :COMMENT
    end

    def init
      @line_number = 0
      advance
      consume_token
    end

  end

end
