module Lisp

  class Port < Atom

    def self.with_value(p)
      self.new(p)
    end

    def initialize(p)
      @value = p
    end

    def type
      :port
    end

    def port?
      true
    end

    def to_s
      "<port: #{@value}>"
    end

  end

end
