module Lisp

  class Atom < Object

    attr_reader :value

    def lisp_object?
      true
    end
    
    def string?
      false
    end

    def character?
      false
    end

    def number?
      false
    end

    def positive?
      false
    end

    def zero?
      false
    end

    def negative?
      false
    end

    def symbol?
      false
    end

    def pair?
      false
    end

    def list?
      false
    end
    
    def primitive?
      false
    end
    
    def special?
      false
    end

    def function?
      false
    end

    def macro?
      false
    end

    def object?
      false
    end

    def class?
      false
    end

    def frame?
      false
    end

    def vector?
      false
    end

    def port?
      false
    end

    def eof_object?
      false
    end

    def environment?
      false
    end

    def length
      1
    end

    def copy
      self.class.new(self.value)
    end
    
    def eqv?(sexpr)
      self == sexpr
    end

    def eq?(sexpr)
      eqv?(sexpr)
    end
    
    def equal?(sexpr)
      eq?(sexpr)
    end
    
    def type
      :unknown
    end

    def evaluate(env)
      self
    end

    def apply_to(args, env)
      nil
    end

    def all?(&block)
      false
    end

    def true?
      true
    end

    def false?
      false
    end

    def car
      nil
    end

    def cdr
      nil
    end

    def quoted
      Lisp::ConsCell.cons(Symbol.named("quote"), self)
    end

    def set!(v)
    end

    def to_s
      @value.to_s
    end

    
    def print_string
      self.to_s
    end

    alias print_string_helper print_string

    
    def doc
      nil
    end
    
  end

end
