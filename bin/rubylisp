#!/usr/bin/env ruby

require 'rubylisp'
require 'rubygems'
require 'commander/import'
require 'readline'
require 'readline/history/restore'

Lisp::Initializer.register_builtins
Lisp::Debug.interactive = true

program :version, '0.0.1'
program :description, 'A large sub/super-set of MIT/GNU-Scheme implemented in pure Ruby.'

# Smarter Readline to prevent empty and dups
#   1. Read a line and append to history
#   2. Quick Break on nil
#   3. Remove from history if empty or dup

def readline_with_hist_management
  line = Readline.readline('> ', true)
  return nil if line.nil?
  if line =~ /^\s*$/ or Readline::HISTORY.to_a[-2] == line
    Readline::HISTORY.pop
  end
  line
end

command :repl do |c|
  c.syntax = 'rubylisp repl [options]'
  c.summary = 'Rubylisp REPL'
  c.description = c.summary
  c.example 'Simply run the REPL', 'rubylisp repl'
  c.example 'Run the REPL, loading a file first', 'rubylisp repl --file code.lsp'
  c.option '-f', '--file LISPFILE', String, 'The name of the lisp file to load'
  c.action do |args, options|
    history_path = File.join(Dir.home, '.rubylisp_history')
    unless File.exist?(history_path)
      File.open(history_path, "w") {|f| }
    end
    Readline::History::Restore.new(history_path)

    options.default :file => ""
    puts 'RubyLisp REPL'
    puts 'Copyright 2014-2015 David R. Astels'
    puts
    parser = Lisp::Parser.new
    parser.process_file(options.file) if options.file != ""
    while line = readline_with_hist_management
      begin 
        puts parser.parse_and_eval(line).print_string
      rescue Exception => ex
        exit if ex.to_s == "exit"
        puts "ERROR: #{ex}"
        # puts ex.backtrace
      end
    end
  end
end

command :test do |c|
  c.syntax = 'rubylisp test [options]'
  c.summary = 'Runs tests'
  c.description = c.summary
  c.example 'Run the tests in tests/list_tests.lsp with verbose output', 'rubylisp test -v -f tests/list_tests.lsp'
  c.example 'Quietly run all tests in the directory tests', 'rubylisp test -q -d tests'
  c.option '-f', '--file TESTFILE', String, 'The name of the test file to run'
  c.option '-d', '--dir TESTDIR', String, 'The name of a directory of test files, all of which will be run'
  c.option '--verbose', "Verbose test output: all context & it labels, and each assertion and it's result."
  c.option '--terse', 'terse test output (the default): + for passes, - for failures, and ! for errors'
  c.option '--quiet', 'Only output a summary'
  c.action do |args, options|
    options.default :file => "",
                    :dir => "",
                    :verbose => false,
                    :quiet => false
    rubylisp_home = ENV['RUBYLISP_HOME'] || "."
    cmd, arg = if options.file != ""
                          ["run-test", File.join(rubylisp_home, options.file)]
                        elsif options.dir != ""
                          ["run-all-tests", File.join(rubylisp_home, options.dir)]
                        else
                          puts "Either a file or directory must be specified"
                          exit
                        end
    verbosity = if options.verbose
                  "'verbose"
                elsif options.quiet
                  "'quiet"
                else
                  ""
                end
    test_command = "(#{cmd} \"#{arg}\" #{verbosity})"
    parser = Lisp::Parser.new
    parser.process_file("#{rubylisp_home}/lisp/testing.lsp") 
    parser.parse_and_eval(test_command).value
  end
end

